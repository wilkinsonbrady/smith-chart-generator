%Equations from: 
%                               https://www.allaboutcircuits.com/technical-articles/mathematical-construction-and-properties-of-the-smith-chart/
function [gama] = smithChartPlot(r,x, label, colorL = 'b', colorM = 'r', labelC = 'r');
  hold on
  %imaginary circles
  gama = (r+(j.*x) -1)./(r+(j.*x) + 1);
  gamaA = real(gama);
  gamaB = imag(gama);
  theta = 0:0.001*pi:2*pi;
  if x==0
      plot([-1,1],[0,0]); %account for x = 0
    else
      radius = abs(1./x); %abs is neccesary, messes with stuff if you allow the radius to go negative
      a = (cos(theta).*radius)+1; %real values
      b = (sin(theta).*radius)+(1./x); %img values
      limit = sqrt((a.^2)+(b.^2));%distance from 0,0
      a0 = sign(1+sign(1-limit)); %create array of 1s for data to keep
      a = (a+1).*a0; %accounts for if sin or cos goes to zero
      b0 = sign(1+sign(1-limit));
      b = (b+1).*b0; %accounts for if sin or cos goes to zero
      a=nonzeros(a);%remove data outside chart
      b=nonzeros(b);
      a = a-1; %undo the + 1
      b = b-1; %undo the + 1
      plot(a,b, colorL,'linewidth',3);
      
      yi = max(abs(b));
      xi = (yi==abs(b));
      yi = (b.*xi);
      xi = (a.*xi);
      xi = nonzeros(xi);
      yi = nonzeros(yi);
      text(xi, yi+(0.02*sign(yi)), num2str(x),'color', labelC, 'fontweight', 'bold', 'HorizontalAlignment', 'center');
      
    end
  %real circles
  radius = (1./(r+1));
  c = (cos(theta).*radius)+(r./(r+1));
  d = (sin(theta).*radius);
  plot(c,d, colorL, 'linewidth', 3);
  
  xi = min(c);
  text(xi-0.01, -0.015, num2str(r),'color', labelC, 'fontweight', 'bold', 'HorizontalAlignment', 'center');
  
  plot(gamaA,gamaB, 'markersize', 28, colorM);  
  text(gamaA, gamaB+(.05*sign(gamaB)), sprintf('%s = %d + %dj',label, gamaA,gamaB), 'HorizontalAlignment', 'center', 'fontsize', 20, 'color', 'm', 'fontweight', 'bold');
  grid on;
  %SWR circle
    radius = abs(gama);
    e = (cos(theta).*radius); %real values
    f = (sin(theta).*radius); %img values
    plot(e,f,'m','linewidth',2);
  fi = min(f);
  text(0, fi-0.03, 'SWR','color', 'm', 'fontweight', 'bold', 'HorizontalAlignment', 'center');
 end
