function smithChart(z) %only does normalized impedance at the moment
  clf
  r = real(z);
  x = imag(z);
  zi = 1./z;
  ri = real(zi);
  xi = imag(zi);
  for i = 0:0.4:4
    gSmithChart(i,i);
  end
  Gama = smithChartPlot(r,x, 'Gama')
  SWR = abs(Gama)
  Admitance = smithChartPlot(ri,xi,'Admittance', 'g', 'c', 'c')
  %Impedance = z
  %Conductance = zi
end
%ToDo: SWR circle, Convert back from normalized impedance
