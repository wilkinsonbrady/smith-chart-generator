function gSmithChart(r=0,x=0,plotR = true,plotX = true,labelC = 'b')
  hold on
  theta = 0:0.001*pi:2*pi;
  %imaginary circles
  if plotX==true
  if x==0
      plot([-1,1],[0,0], 'linewidth', 2,'k'); %account for x = 0  
      plot(0,0, 'k', 'markersize', 20);
      text(-0.01, -0.015, '0', 'color', labelC);
    else
      radius = abs(1./x); %abs is neccesary messes with stuff if you allow the radius to go negative
      a = (cos(theta).*radius)+1;
      b = (sin(theta).*radius)+(1./x);
      limit = sqrt((a.^2)+(b.^2));
      a0 = sign(1+sign(1-limit));
      a = (a+1).*a0; %accounts for if sin or cos goes to zero
      b0 = sign(1+sign(1-limit));
      b = (b+1).*b0; %accounts for if sin or cos goes to zero
      a=nonzeros(a);
      b=nonzeros(b);
      a = a-1; %undo the + 1
      b = b-1; %undo the + 1
      plot(a,b, 'k', 'linewidth', 1);
      plot(a,-b, 'k', 'linewidth', 1);
      
      yi = max(abs(b));
      xi = (yi==abs(b));
      xi = (a.*xi);
      xi = nonzeros(xi);
      text(xi, yi+0.02, num2str(x),'color', labelC, 'fontweight', 'bold', 'HorizontalAlignment', 'center');
      text(xi, -yi-0.02, num2str(-x),'color', labelC, 'fontweight', 'bold', 'HorizontalAlignment', 'center');    
    
  end
 end
  %real circles
  if plotR==true
    radius = (1./(r+1));
    c = (cos(theta).*radius)+(r./(r+1));
    d = (sin(theta).*radius);
    plot(c,d, 'k', 'linewidth', 1);    
  
    xi = min(c);
    text(xi-0.01, -0.015, num2str(r),'color', labelC, 'fontweight', 'bold', 'HorizontalAlignment', 'center');
  end
  set(gca, 'XTickLabel', [], 'YTickLabel', [])
  grid on;
end
